const config = require('config');
const determinant = require('../modules/determinant');

const months = config.get('enums.months');

exports.get_weekday = function(date) {
    return (date.getDay() + 6) % 7;
};

exports.is_today = function(first_date, second_date) {
    return first_date.getFullYear() === second_date.getFullYear() &&
        first_date.getMonth() === second_date.getMonth() &&
        first_date.getDay() === second_date.getDay();
};

// Проверка корректности дня недели
// Возвращает универсальный id дня недели при успешности
exports.weekday_is_correct = function (on_weekday) {
    let weekday_num;
    let date_now = new Date();
    date_now.setHours(date_now.getUTCHours() + config.get('settings.utc'));
    switch (on_weekday) {
        case 'today':
            weekday_num = exports.get_weekday(date_now);
            break;
        case 'tomorrow':
            weekday_num = (exports.get_weekday(date_now) + 1) % 7;
            break;
        default:
            weekday_num = determinant.weekday(on_weekday);
            break;
    }

    if (weekday_num === null || weekday_num === undefined)
        return null;

    return weekday_num;
};

exports.get_readable_date = function (date) {
    return date.getDate() + ' ' + months[date.getMonth()]['ru_gen'].toUpperCase();
};

// Проверяет номер группы на существование
exports.group_number_is_correct = async function (chat, group_number) {
    group_number = group_number.toLowerCase();
    let group_list = (await require('../modules/caching/schedules').load_group_list(chat))['data'];
    if (!group_list) {
        require('../modules/bot/sender').send_message(chat, config.get('Telegram_Bot.messages.load_from_server_failure'));
        return null;
    }

    let groups = group_list['groups'].map(function(x){ return x.toLowerCase() });
    let index = groups.indexOf(group_number);
    return (index) ? group_list['groups'][index] : false;
};

exports.get_timestamp_date_for_db = function (date) {
    return date.toISOString().slice(0, 19).replace('T', ' ');
};