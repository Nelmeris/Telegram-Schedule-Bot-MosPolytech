const utilities = require('../modules/utilities');
const config = require('config');

/** Временные переменные **/
const weekdays = config.get('enums.weekdays');
const months = config.get('enums.months');

exports.schedule = function (username, is_actual, group_number, date) {
    let msg = config.get('console_log_messages.schedule_sent');
    msg = msg.replace('{username}', username);
    msg = msg.replace('{is_actual}', (is_actual) ? 'actual' : 'not-actual');
    msg = msg.replace('{group_number}', group_number);
    let on_msg;
    if (date) {
        let weekday_num = utilities.get_weekday(date);
        let weekday_en = weekdays[weekday_num]['en'];
        let month = months[date.getMonth()]['en'];
        on_msg = 'on ' + month + ' ' + date.getDate() + ', ' + weekday_en;
    } else
        on_msg = 'on all week';
    msg = msg.replace('{on}', on_msg);
    console.log(msg);
};

exports.error = function (username, message) {
    console.log('@' + username + ' ' + message);
};

exports.action = function (username, message) {
    console.log('@' + username + ' ' + message);
};
