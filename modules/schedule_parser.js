const config = require('config');
const determinant = require('../modules/determinant');

exports.parse = function (html) {
    let schedule = {};

    let days = html.querySelectorAll(config.get('HTML_Selector.schedule_day'));
    for (let i = 0; i < days.length; i++) {
        let day = days[i];

        let weekday = determinant.weekday(day.querySelector(config.get('HTML_Selector.weekday_title')).text);
        let pairs = day.querySelector(config.get('HTML_Selector.schedule_pairs'));
        schedule[weekday] = parse_pairs(pairs);
    }

    return schedule;
};

function parse_pairs(html) {
    let schedule = [];

    let pairs = html.querySelectorAll(config.get('HTML_Selector.schedule_pair'));
    for (let i = 0; i < pairs.length; i++) {
        let pair = pairs[i];
        schedule[i] = {};

        schedule[i]['time'] = pair.querySelector(config.get('HTML_Selector.pair_time')).text;
        let lessons = pair.querySelector(config.get('HTML_Selector.pair_lessons'));
        schedule[i]['lessons'] = parse_lessons(lessons);
    }

    return schedule;
}

function parse_lessons(html) {
    let schedule = [];

    let lessons = html.querySelectorAll(config.get('HTML_Selector.pair_lesson'));
    for (let i = 0; i < lessons.length; i++) {
        let lesson = lessons[i];
        schedule[i] = {};

        schedule[i]['title'] = lesson.querySelector(config.get('HTML_Selector.pair_lesson_title')).text;
        schedule[i]['auditories'] = parse_auditories(lesson.querySelectorAll(config.get('HTML_Selector.pair_lesson_auditory')));
        schedule[i]['teachers'] = parse_teachers(lesson.querySelector(config.get('HTML_Selector.pair_lesson_teacher')));
        schedule[i]['dates'] = parse_dates(lesson.querySelector(config.get('HTML_Selector.pair_lesson_dates')));
    }

    return schedule;
}

function parse_auditories(html) {
    let auditories = [];
    for (let i = 0; i < html.length; i++)
        auditories[i] = html[i].text;
    return auditories;
}

function parse_teachers(html) {
    let teachers = html.text;
    if (!teachers)
        return null;
    return teachers.split(', ');
}

function parse_dates(html) {
    let parsed_dates = {};

    let dates = html.text.split(' - ');
    let date_now = new Date();
    date_now.setHours(date_now.getUTCHours() + config.get('settings.utc'));
    let year = date_now.getFullYear();

    let date = dates[0].split(' ');
    let day = parseInt(date[0]);
    let month = determinant.month(date[1]);

    if (dates.length > 1) {
        parsed_dates['date-from'] = new Date(year, month, day);

        date = dates[1].split(' ');
        day = parseInt(date[0]);
        month = determinant.month(date[1]);

        parsed_dates['date-to'] = new Date(year, month, day);
    } else
        parsed_dates['date-on'] = new Date(year, month, day);

    return parsed_dates;
}