const config = require('config');

const management = require('../bot/management');
const sender = require('../bot/sender');
const logger = require('../logger');

const Enum = require('enum');

/** Перечисление команд **/
const BOT_COMMANDS = new Enum(config.get('enums.bot_commands'));
const MAIN_MENU_COMMANDS = new Enum(config.get('enums.main_menu_buttons'));
const STATES = new Enum(config.get('enums.states'));

let bot;

/** Инициализация кэша **/
const chats_cache = require('../caching/chats');

exports.start = function (bot_link) {
    bot = bot_link;
    management.set_bot(bot);
    sender.set_bot(bot);

    /** Main commands Handlers **/
    const regex_menu_change_group = new RegExp('^' + MAIN_MENU_COMMANDS.CHANGE_GROUP.value + '$');
    bot.onText(regex_menu_change_group, function (msg) {
        let chat = msg.chat;
        (async () => {
            if (!await management.check_auth(chat))
                return;
            await management.close_group(chat);
            management.show_enter_group_number(chat);
        })();
    });
    const regex_menu_schedule_general = new RegExp('^' + MAIN_MENU_COMMANDS.GENERAL.value + '$');
    bot.onText(regex_menu_schedule_general, function (msg) {
        let chat = msg.chat;
        (async () => {
            if (!await management.check_auth(chat))
                return;
            management.show_general_schedule(chat);
        })()
    });
    const regex_menu_schedule_on_week = new RegExp('^' + MAIN_MENU_COMMANDS.ON_WEEK.value + '$');
    bot.onText(regex_menu_schedule_on_week, function (msg) {
        let chat = msg.chat;
        (async () => {
            if (!await management.check_auth(chat))
                return;
            management.show_schedule_on_week(chat);
        })()
    });
    const regex_menu_schedule_on_today = new RegExp('^' + MAIN_MENU_COMMANDS.ON_TODAY.value + '$');
    bot.onText(regex_menu_schedule_on_today, function (msg) {
        let chat = msg.chat;
        (async () => {
            if (!await management.check_auth(chat))
                return;
            management.show_schedule_on_today(chat);
        })()
    });
    const regex_menu_schedule_on_tomorrow = new RegExp('^' + MAIN_MENU_COMMANDS.ON_TOMORROW.value + '$');
    bot.onText(regex_menu_schedule_on_tomorrow, function (msg) {
        let chat = msg.chat;
        (async () => {
            if (!await management.check_auth(chat))
                return;
            management.show_schedule_on_tomorrow(chat);
        })();
    });
    const regex_menu_schedule_on_weekday = new RegExp('^' + MAIN_MENU_COMMANDS.ON_WEEKDAY.value + '$');
    bot.onText(regex_menu_schedule_on_weekday, function (msg) {
        let chat = msg.chat;
        (async () => {
            if (!await management.check_auth(chat))
                return;
            management.show_choice_weekday(chat);
        })();
    });
    const regex_menu_send_feedback = new RegExp('^' + MAIN_MENU_COMMANDS.SEND_FEEDBACK.value + '$');
    bot.onText(regex_menu_send_feedback, function (msg) {
        sender.send_feedback(msg.chat);
    });

    /** Обработка кнопок и текстов **/
    bot.on('callback_query', function (msg) {
        let chat = msg.from;
        // Кнопка выбора группы
        if (!chats_cache.state_is_equal(chat, STATES.ENTER_GROUP) && !chats_cache.state_is_equal(chat, STATES.INACTIVE))
            return;
        let group_number = msg.data;
        let regex = new RegExp(config.get('regex_patterns.group_number'));
        if (group_number.match(regex))
            management.open_group(chat, group_number);
    });
    bot.on('callback_query', function (msg) {
        let chat = msg.from;
        // Кнопка отображения полного расписания
        if (msg.data.includes('detail')) {
            let detail = JSON.parse(msg.data)['detail'];
            if (!management.check_auth(chat))
                return;
            let group_number = detail['group_number'];
            let weekday = detail['weekday_number'];

            if (chats_cache.get_group_for_chat(chat) === group_number)
                sender.send_schedule(chat, group_number, false, weekday);
        }
    });
    bot.on('callback_query', function (msg) {
        let chat = msg.from;
        // Кнопка выбора дня недели
        if (chats_cache.state_is_equal(chat, STATES.CHOICE_WEEKDAY) && !msg.data.includes('detail')) {
            if (!management.check_auth(chat))
                return;
            management.show_schedule_on_weekday(chat, msg.data);
        }
    });
    bot.on('callback_query', function (msg) {
        let chat = msg.from;
        if (msg.data === 'feedback') {
            bot.sendMessage(chat.id, 'Спасибо за отзыв!');
            chats_cache.set_feedback_state(chat);
            logger.action(chat.username, 'sent feedback');
        }
    });

    bot.on('message', function (msg) {
        if (!chats_cache.is_new_chat(msg.chat)) return;
        let message = config.get('Telegram_Bot.messages.welcome');
        sender.send_message(msg.chat, message);
    });

    bot.on('message', function (msg) {
        let chat = msg.chat;
        let text = msg.text.trim();
        let is_command = false;
        BOT_COMMANDS.enums.forEach(function(enumItem) {
            if (enumItem.value === text) {
                is_command = true;
                return;
            }
        });
        MAIN_MENU_COMMANDS.enums.forEach(function (enumItem) {
            if (enumItem.value === text) {
                is_command = true;
                return;
            }
        });
        if ((!chats_cache.state_is_equal(chat, STATES.ENTER_GROUP) &&
            !chats_cache.state_is_equal(chat, STATES.INACTIVE))
            || is_command)
            return;

        if (!text.match(new RegExp(config.get('regex_patterns.group_number')))) {
            if (chats_cache.state_is_equal(chat, STATES.ENTER_GROUP))
                bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.incorrect_input_data'));
            return;
        }

        (async () => {
            let group_is_valid = await require('../../modules/utilities').group_number_is_correct(chat, text);

            if (group_is_valid === null) {
                chats_cache.set_chat_state(chat, STATES.INACTIVE);
                return;
            }
            if (!group_is_valid) {
                bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.group_search_failure'));
                return;
            }
            management.open_group(chat, group_is_valid);
        })();
    });

    /** Bot commands **/
    const regex_start_command = new RegExp(BOT_COMMANDS.START.value + '|' + BOT_COMMANDS.SCHEDULE.value);
    bot.onText(regex_start_command, function (msg) {
        let chat = msg.chat;
        let state = (chats_cache.get_group_for_chat(chat)) ? STATES.MAIN_MENU : STATES.ENTER_GROUP;

        switch (state) {
            case STATES.MAIN_MENU:
                management.show_main_menu(chat);
                break;
            case STATES.ENTER_GROUP:
                management.show_enter_group_number(chat);
                break;
        }
    });

    const regex_change_group = new RegExp(BOT_COMMANDS.CHANGE_GROUP.value);
    bot.onText(regex_change_group, function (msg) {
        let chat = msg.chat;
        (async () => {
            if (await management.check_auth(chat))
                await management.close_group(chat);
            management.show_enter_group_number(chat);
        })();
    });

    /** Универсальная команда **/
    const regex_command_of_schedule = new RegExp(config.get('regex_patterns.universal_command_of_schedule'));
    bot.onText(regex_command_of_schedule, function (msg, match) {
        management.universal_command(msg, match);
    });
};
