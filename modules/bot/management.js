const determinant = require('../determinant');
const chats_cache = require('../caching/chats');
let bot;

exports.set_bot = function (bot_link) {
    bot = bot_link;
};

const sender = require('../../modules/bot/sender');
const logger = require('../../modules/logger');

const Enum = require('enum');

/** Подключение модулей **/
const { InlineKeyboard, ReplyKeyboard } = require("telegram-keyboard-wrapper");

const config = require('config');

const utilities = require("../utilities");

const STATES = new Enum(config.get('enums.states'));
const MAIN_MENU_COMMANDS = new Enum(config.get('enums.main_menu_buttons'));

/** Клавиатуры **/
const weekday_keyboard = new InlineKeyboard();
weekday_keyboard.addRow(
    { text: 'Понедельник', callback_data: 'mon' },
    { text: 'Вторник', callback_data: 'tue' }
);
weekday_keyboard.addRow(
    { text: 'Среда', callback_data: 'wed' },
    { text: 'Четверг', callback_data: 'thu' }
);
weekday_keyboard.addRow(
    { text: 'Пятница', callback_data: 'fri' },
    { text: 'Суббота', callback_data: 'sat' }
);
weekday_keyboard.addRow(
    { text: 'Воскресенье', callback_data: 'sun' }
);

const main_menu_commands_keyboard = new ReplyKeyboard();
main_menu_commands_keyboard.addRow(MAIN_MENU_COMMANDS.ON_TODAY.value, MAIN_MENU_COMMANDS.ON_TOMORROW.value);
main_menu_commands_keyboard.addRow(MAIN_MENU_COMMANDS.ON_WEEKDAY.value, MAIN_MENU_COMMANDS.ON_WEEK.value);
main_menu_commands_keyboard.addRow(MAIN_MENU_COMMANDS.GENERAL.value);
main_menu_commands_keyboard.addRow(MAIN_MENU_COMMANDS.CHANGE_GROUP.value, MAIN_MENU_COMMANDS.SEND_FEEDBACK.value);

/** Работа с авторизацией **/
exports.check_auth = async function (chat) {
    if (chats_cache.get_group_for_chat(chat))
        return true;
    await bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.unauthorized'), main_menu_commands_keyboard.close());
    return false;
};
exports.close_group = async function (chat) {
    logger.action(chat.username, 'close group');
    chats_cache.set_group_for_chat(chat, null);
    await bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.close_group_schedule'), main_menu_commands_keyboard.close());
};
exports.open_group = function (chat, group_number) {
    logger.action(chat.username, 'open ' + group_number + ' group');
    chats_cache.set_group_for_chat(chat, group_number);
    exports.show_main_menu(chat);
};

// Открытие окна ввода номера группы
exports.show_enter_group_number = async function (chat) {
    logger.action(chat.username, 'started enter the number of the group');
    chats_cache.set_chat_state(chat, STATES.ENTER_GROUP);
    let message = config.get('Telegram_Bot.messages.prompt_to_enter_group_number');
    let options;
    let group_history = chats_cache.get_group_history(chat);
    if (group_history && group_history.length > 0) {
        message += config.get('Telegram_Bot.messages.prompt_to_choice_group_number');
        let keyboard = new InlineKeyboard();
        keyboard.addRow([]);
        for (let i = group_history.length - 1; i >= 0; i--)
            keyboard.push(0,{ text: group_history[i], callback_data: group_history[i] });
        options = keyboard.export();
    }
    if (!options)
        options = main_menu_commands_keyboard.close();
    await bot.sendMessage(chat.id, message, options);
};

/** Авторизованные команды **/
exports.show_main_menu = function (chat) {
    logger.action(chat.username, 'open main menu');
    chats_cache.set_chat_state(chat, STATES.MAIN_MENU);
    let group_number = chats_cache.get_group_for_chat(chat);
    let message = config.get('Telegram_Bot.messages.welcome_to_main_menu').replace('{group_number}', '<b>' + group_number + '</b>');
    let options = main_menu_commands_keyboard.open();
    options['parse_mode'] = config.get('Telegram_Bot.text_formatting_type');
    bot.sendMessage(chat.id, message, options);
};
exports.show_general_schedule = function (chat) {
    let group_number = chats_cache.get_group_for_chat(chat);
    chats_cache.set_schedule_day_for_chat(chat, null);
    sender.send_schedule(chat, group_number, false, null);
};
exports.show_schedule_on_week = function (chat) {
    let group_number = chats_cache.get_group_for_chat(chat);
    chats_cache.set_schedule_day_for_chat(chat, null);
    sender.send_schedule(chat, group_number, true, null);
};
exports.show_schedule_on_today = function (chat) {
    let group_number = chats_cache.get_group_for_chat(chat);
    let date = new Date();
    date.setHours(date.getUTCHours() + config.get('settings.utc'));
    let weekday = utilities.get_weekday(date);

    chats_cache.set_schedule_day_for_chat(chat, weekday);

    sender.send_schedule(chat, group_number, true, weekday, true);
};
exports.show_schedule_on_tomorrow = function (chat) {
    let group_number = chats_cache.get_group_for_chat(chat);
    let date = new Date();
    date.setHours(date.getUTCHours() + config.get('settings.utc'));
    date.setDate(date.getDate() + 1);
    let weekday = utilities.get_weekday(date);

    chats_cache.set_schedule_day_for_chat(chat, weekday);

    sender.send_schedule(chat, group_number, true, weekday);
};
exports.show_choice_weekday = function (chat) {
    logger.action(chat.username, 'open weekday choicer');
    chats_cache.set_chat_state(chat, STATES.CHOICE_WEEKDAY);
    bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.prompt_to_enter_weekday'), weekday_keyboard.export());
};
exports.show_schedule_on_weekday = function (chat, weekday) {
    let weekday_num = determinant.weekday(weekday);
    chats_cache.set_schedule_day_for_chat(chat, weekday_num);
    let group_number = chats_cache.get_group_for_chat(chat);
    sender.send_schedule(chat, group_number, true, weekday_num);
};
exports.universal_command = function (msg, match) {
    let chat = msg.chat;

    let group_number = match[1];
    (async () => {
        let group_is_valid = await group_number_is_correct(group_number, chat);

        if (group_is_valid === null)
            return;
        if (!group_is_valid) {
            bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.group_search_failure'));
            return;
        }

        let on_weekday = match[6];
        let weekday_num = null;
        if (on_weekday) {
            weekday_num = utilities.weekday_is_correct(on_weekday);
            if (weekday_num === null) {
                bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.incorrect_input_data'));
                return;
            }
        }

        let is_actual = !match[3] || match[3] && match[3] === '-actual';

        logger.schedule(chat.username, is_actual, group_is_valid, on_weekday);
        await sender.send_schedule(chat, group_is_valid, is_actual, weekday_num);

        chats_cache.set_chat_state(chat, STATES.INACTIVE);
    })();

};
