const config = require('config');

const utilities = require('../../modules/utilities');
const { InlineKeyboard } = require("telegram-keyboard-wrapper");
const chats_cache = require('../../modules/caching/chats');
const schedules_cache = require('../../modules/caching/schedules');
const schedule_creator = require('../../modules/schedule_creator');

const logger = require('../../modules/logger');

let bot;
const weekdays = config.get('enums.weekdays');

exports.set_bot = function (bot_link) {
    bot = bot_link;
};

exports.send_message = async function (chat, message, options) {
    try {
        await bot.sendMessage(chat.id, message, options);
    } catch (error) {
        logger.error(chat.username, error.message);
    }
};

exports.send_feedback = async function (chat) {
    let feedback_msg = config.get('Telegram_Bot.messages.feedback');
    let feedback_url = config.get('settings.feedback_form_url');
    feedback_msg = feedback_msg.replace('{url}', feedback_url);
    let keyboard = new InlineKeyboard();
    keyboard.addRow({ text: 'Отправил(а) :)', callback_data: 'feedback' });
    chats_cache.set_feedback_sent_date(chat);
    let options = keyboard.export();
    options['disable_web_page_preview'] = true;
    exports.send_message(chat, feedback_msg, options);
};

exports.send_welcome = async function (chat) {
    exports.send_message(chat, config.get('Telegram_Bot.messages.welcome'));
};

// Отправление расписания в чат
exports.send_schedule = async function (chat, group_number, is_actual, weekday_num, is_today) {
    let data = await schedules_cache.load_schedule(chat, group_number);
    if (!data)
        await exports.send_message(chat, config.get('Telegram_Bot.messages.load_from_server_failure'));

    let keyboard = new InlineKeyboard();
    let callback_data = {
        detail: {
            group_number: group_number,
            weekday_number: weekday_num
        }
    };
    keyboard.addRow({
        text: config.get('Telegram_Bot.messages.show_detail_schedule'),
        callback_data: JSON.stringify(callback_data)
    });
    let options = { parse_mode: config.get('Telegram_Bot.text_formatting_type') };
    let date = new Date();
    date.setHours(date.getUTCHours() + config.get('settings.utc'));

    const no_pairs_on_day_message = config.get('Telegram_Bot.messages.no_pairs_on_day');

    if (weekday_num === null || weekday_num === undefined) {
        let flag = false;
        let weekday_now = utilities.get_weekday(date);
        date.setDate(date.getDate() - weekday_now);

        for (let [weekday_num, day] of Object.entries(data)) {
            let msg_day = schedule_creator.on_weekday(day, weekday_num, is_actual);
            if (msg_day !== no_pairs_on_day_message) {
                flag = true;
                let weekday_ru = weekdays[weekday_num];
                let schedule = '<pre>';
                if (is_actual)
                    schedule += utilities.get_readable_date(date) + ', ';
                schedule += weekday_ru['ru'].toUpperCase() + '</pre>\n\n';
                schedule += msg_day + '\n';
                await exports.send_message(chat, schedule, options);
            }
            date.setDate(date.getDate() + 1);
        }

        if (!flag)
            await exports.send_message(chat, no_pairs_on_day_message, options);

        logger.schedule(chat.username, is_actual, group_number);
    } else {
        let weekday_now = utilities.get_weekday(date);
        let new_date = date.getDate() + ((weekday_num >= weekday_now) ? weekday_num - weekday_now : 7 - (weekday_now - weekday_num));
        date.setDate(new_date);
        logger.schedule(chat.username, is_actual, group_number, date);
        let schedule = (await schedule_creator.on_weekday(data[weekday_num], weekday_num, is_actual, is_today));
        if (schedule) {
            let schedule_title = '<b>РАСПИСАНИЕ НА ';
            if (is_actual) {
                schedule_title += utilities.get_readable_date(date) + ', ';
                options = {...options, ...keyboard.export()};
            }
            schedule_title += weekdays[utilities.get_weekday(date)]['ru'].toUpperCase() + '</b>\n';
            schedule = schedule_title + schedule;
            await exports.send_message(chat, schedule, options);
        }
    }
};
