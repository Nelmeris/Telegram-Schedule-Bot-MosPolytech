const { Pool } = require('pg');

const config = require('config');
const utilities = require('../../modules/utilities');

const pool = new Pool({
    user: config.get('db_settings.user'),
    host: config.get('db_settings.host'),
    database: config.get('db_settings.database'),
    password: config.get('db_settings.password'),
    port: config.get('db_settings.port'),
    ssl: config.get('db_settings.ssl')
});

exports.pull_chats = async function() {
    const chats_info = config.get('db_settings.schema') + '.' + config.get('db_settings.chats_info_table_title');
    const chats_cache = config.get('db_settings.schema') + '.' + config.get('db_settings.chats_table_title');
    const chats_stats = config.get('db_settings.schema') + '.' + config.get('db_settings.chats_statistic_table_title');
    const sql = `
    SELECT * FROM ${chats_info} info
    INNER JOIN ${chats_cache} cache
        ON info.id = cache.id
    INNER JOIN ${chats_stats} stats
        ON info.id = stats.chat_id`;
    try {
        let response = await pool.query(sql);
        let result = {};
        for (let i = 0; i < response.rows.length; i++) {
            let row = response['rows'][i];
            let selected_group = row['selected_group'];
            let group_history = row['group_history'];
            let state = row['state'];
            let is_feedback_sent = row['is_feedback_sent'];
            let feedback_date = ((row['feedback_date']) ? new Date(row['feedback_date']) : null);
            let last_activity = ((row['last_activity']) ? new Date(row['last_activity']) : null);
            let username = row['username'];
            let first_name = row['first_name'];
            let last_name = row['last_name'];
            result[row['chat_id']] = {
                username: username,
                first_name: first_name,
                last_name: last_name,
                selected_group: selected_group,
                last_groups: group_history,
                state: state,
                is_feedback_sent: is_feedback_sent,
                feedback_date: feedback_date,
                last_activity: last_activity
            };
        }
        return result;
    } catch (error) {
        console.log(error);
    }
};

exports.pull_schedules = async function() {
    const table_name = config.get('db_settings.schema') + '.' + config.get('db_settings.schedules_table_title');
    const sql = `SELECT * FROM ${table_name}`;
    let result = {};
    try {
        let response = await pool.query(sql);
        for (let i = 0; i < response.rows.length; i++) {
            let row = response['rows'][i];
            result[row['group_number']] = {
                data: {
                    0: row['monday'],
                    1: row['tuesday'],
                    2: row['wednesday'],
                    3: row['thursday'],
                    4: row['friday'],
                    5: row['saturday'],
                    6: row['sunday'],
                },
                load_date: new Date(row['update_date'])
            };
        }
        return result;
    } catch (error) {
        console.log(error);
    }
};

exports.push_chats = async function(id, chat) {
    const chats_cache_table_name = config.get('db_settings.schema') + '.' + config.get('db_settings.chats_table_title');
    const group_number = (chat['selected_group']) ? ("'" + chat['selected_group'] + "'") : 'null';
    const group_history = (chat['last_groups']) ? ("ARRAY['" + chat['last_groups'].join("', '") + "']") : 'null';
    const state = (chat['state']) ? ("'" + chat['state'] + "'") : 'null';
    let sql = `
        INSERT INTO ${chats_cache_table_name}
            VALUES (${id}, ${group_number}, ${group_history}, ${state})
        ON CONFLICT (id) DO
            UPDATE SET
            selected_group = ${group_number},
            group_history = ${group_history},
            state = ${state};
    `;

    const chats_info_table_name = config.get('db_settings.schema') + '.' + config.get('db_settings.chats_info_table_title');
    const chat_username = (chat['username']) ? ("'" + chat['username'] + "'") : 'null';
    const chat_first_name = (chat['first_name']) ? ("'" + chat['first_name'] + "'") : 'null';
    const chat_last_name = (chat['last_name']) ? ("'" + chat['last_name'] + "'") : 'null';
    sql += `
        INSERT INTO ${chats_info_table_name}
            VALUES (${id}, ${chat_username}, ${chat_first_name}, ${chat_last_name})
        ON CONFLICT (id) DO
            UPDATE SET
            username = ${chat_username},
            first_name = ${chat_first_name},
            last_name = ${chat_last_name};         
    `;

    const chats_statistic_table_name = config.get('db_settings.schema') + '.' + config.get('db_settings.chats_statistic_table_title');
    const is_feedback_sent = (chat['is_feedback_sent']) ? (chat['is_feedback_sent']) : false;
    const feedback_date = (chat['feedback_date']) ? "'" + (utilities.get_timestamp_date_for_db(chat['feedback_date'])) + "'" : 'null';
    const last_activity = (chat['last_activity']) ? "'" + (utilities.get_timestamp_date_for_db(chat['last_activity'])) + "'" : 'null';
    sql += `
        INSERT INTO ${chats_statistic_table_name}
            VALUES (${id}, ${is_feedback_sent}, ${feedback_date}, ${last_activity})
        ON CONFLICT (chat_id) DO
            UPDATE SET
            is_feedback_sent = ${is_feedback_sent},
            feedback_date = ${feedback_date},
            last_activity = ${last_activity};
    `;

    try {
        await pool.query(sql);
    } catch (error) {
        console.log(error);
    }
};

exports.push_schedules = async function(group_number, schedules) {
    const table_name = config.get('db_settings.schema') + '.' + config.get('db_settings.schedules_table_title');
    let schedule_sql = '';
    for (let i = 0; i < 7; i++) {
        let schedule = schedules['data'][i];
        if (i !== 0)
            schedule_sql += ', ';
        if (!schedule) {
            schedule_sql += 'null';
            continue;
        }
        schedule_sql += "ARRAY[";
        for (let j = 0; j < schedule.length; j++) {
            if (j !== 0)
                schedule_sql += ', ';
            schedule_sql += "'" + JSON.stringify(schedule[j]) + "'";
        }
        schedule_sql += "]::json[]";
    }
    const date = utilities.get_timestamp_date_for_db(new Date(schedules['load_date']));

    let update_sql = '';
    let weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    for (let i = 0; i < 7; i++) {
        let schedule = schedules['data'][i];
        if (i !== 0)
            update_sql += ', ';
        update_sql += weekdays[i] + ' = ';
        if (!schedule) {
            update_sql += 'null';
            continue;
        }
        update_sql += "ARRAY[";
        for (let j = 0; j < schedule.length; j++) {
            if (j !== 0)
                update_sql += ', ';
            update_sql += "'" + JSON.stringify(schedule[j]) + "'";
        }
        update_sql += "]::json[]";
    }

    const sql = `
        INSERT INTO ${table_name}
            VALUES ('${group_number}', ${schedule_sql}, '${date}')
        ON CONFLICT (group_number) DO
            UPDATE SET
            ${update_sql},
            update_date = '${date}'
    `;

    try {
        await pool.query(sql);
    } catch (error) {
        console.log(error);
    }
};
