const db = require('./database');

const Enum = require('enum');
const config = require('config');
const STATES = new Enum(config.get('enums.states'));

let chats_cache = {};

exports.pull_from_db = async function () {
    chats_cache = await db.pull_chats();
};

const state_key = 'state';
const is_feedback = 'is_feedback_sent';
const feedback_date = 'feedback_date';
const schedule_day = 'schedule_day';
const selected_group = 'selected_group';
const last_groups = 'last_groups';

exports.is_new_chat = function(chat) {
    return !chats_cache[chat.id];
};

/** Инициализация кэша **/
function create_chat_cache_if_needed (chat) {
    if (chats_cache[chat.id])
        return;
    chats_cache[chat.id] = {};
    chats_cache[chat.id][state_key] = STATES.INACTIVE;
}

/** Управление данными **/
function set_to_chat_cache(chat, key, value) {
    create_chat_cache_if_needed(chat);
    chats_cache[chat.id][key] = value;
    chats_cache[chat.id]['username'] = chat.username;
    chats_cache[chat.id]['first_name'] = chat.first_name;
    chats_cache[chat.id]['last_name'] = chat.last_name;
    chats_cache[chat.id]['last_activity'] = new Date();
    db.push_chats(chat.id, chats_cache[chat.id]);
}
function get_from_chat_cache(chat, key) {
    create_chat_cache_if_needed(chat);
    return chats_cache[chat.id][key];
}

/** Состояние **/
exports.state_is_equal = function (chat, state) {
    return get_chat_state(chat) === state.key;
};
function get_chat_state (chat) {
    return get_from_chat_cache(chat, state_key);
}
exports.set_chat_state = function (chat, state) {
    set_to_chat_cache(chat, state_key, state.key);
};

/** История последних групп **/
function add_group_to_history (chat, group_number) {
    create_chat_cache_if_needed(chat);
    let cache = get_from_chat_cache(chat, last_groups);
    if (!cache) cache = [];

    if (cache.includes(group_number)) {
        let index_st = cache.indexOf(group_number);
        cache.splice(index_st, 1);
    }

    if (cache.length === 3)
        cache.shift();

    cache.push(group_number);
    set_to_chat_cache(chat, last_groups, cache);
}
exports.get_group_history = function (chat) {
    return get_from_chat_cache(chat, last_groups);
};

/** Выбранная группа **/
exports.set_group_for_chat = function (chat, group_number) {
    set_to_chat_cache(chat, selected_group, group_number);
    if (group_number)
        add_group_to_history(chat, group_number);
};
exports.get_group_for_chat = function (chat) {
    return get_from_chat_cache(chat, selected_group);
};

/** День расписания **/
exports.set_schedule_day_for_chat = function (chat, day) {
    set_to_chat_cache(chat, schedule_day, day);
};
exports.get_schedule_day_for_chat = function (chat) {
    return get_from_chat_cache(chat, schedule_day);
};

/** Состояние отправки отзыва **/
exports.set_feedback_state = function (chat) {
    set_to_chat_cache(chat, is_feedback, true);
};
exports.get_feedback_state = function (chat) {
    return get_from_chat_cache(chat, is_feedback);
};

/** Дата автоматического отправления предложения об отзыве **/
exports.set_feedback_sent_date = function (chat) {
    let date = new Date();
    date.setHours(date.getUTCHours() + config.get('settings.utc'));
    set_to_chat_cache(chat, feedback_date, date);
};
exports.get_feedback_sent_date = function (chat) {
    return get_from_chat_cache(chat, feedback_date);
};
