const config = require('config');
let bot;

let schedule_cache = {};
let group_list_cache = {};

const db = require('../caching/database');
const loader = require('../mospolytech_loader');

exports.set_bot = function (bot_link) {
    bot = bot_link;
};

exports.pull_from_db = async function () {
    schedule_cache = await db.pull_schedules();
};

exports.load_schedule = async function (chat, group_number) {
    let data = exports.schedule_from_cache(group_number);
    if (data)
        return data['data'];

    bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.load_schedule_from_server_process'));
    data = await loader.schedule_from_server(group_number);
    if (data) {
        schedule_cache[group_number] = data;
        db.push_schedules(group_number, data);
        return data['data'];
    }

    return null;
};
exports.load_group_list = async function (chat) {
    let data = exports.group_list_from_cache();
    if (data)
        return data;

    bot.sendMessage(chat.id, config.get('Telegram_Bot.messages.load_group_list_from_server_process'));
    data = await loader.group_list_from_server();
    if (data) {
        group_list_cache = data;
        return data;
    }

    return null;
};

exports.schedule_from_cache = function(group) {
    if (schedule_on_cache_is_valid(group))
        return schedule_cache[group];
    else
        return null;
};
exports.group_list_from_cache = function() {
    if (group_list_on_cache_is_valid())
        return group_list_cache;
    else
        return null;
};

function schedule_on_cache_is_valid (group) {
    if (!schedule_cache[group])
        return false;

    let cache = schedule_cache[group];

    let schedule_load_date = new Date(cache['load_date']);
    let schedule_life_time = config.get('MosPolytech.schedule_life_time');
    schedule_load_date = new Date(schedule_load_date.getTime() + schedule_life_time * 60000);
    let date_now = new Date();
    return cache['data'] && date_now < schedule_load_date;
}

function group_list_on_cache_is_valid () {
    if (!group_list_cache)
        return false;
    let group_list_live_time = config.get('MosPolytech.group_list_life_time');
    let date = new Date(new Date(group_list_cache['update_date']).getTime() + group_list_live_time * 60000);
    let date_now = new Date();
    return (date_now < date);
}