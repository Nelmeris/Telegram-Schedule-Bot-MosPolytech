const config = require('config');
const utilities = require('../modules/utilities');

const no_pairs_on_day_message = config.get('Telegram_Bot.messages.no_pairs_on_day');
const no_pairs_on_today_message = config.get('Telegram_Bot.messages.no_pairs_on_today');

exports.on_weekday = function(schedule, weekday_num, is_actual, is_today) {
    if (!schedule) return no_pairs_on_day_message;
    let msg = '';
    let flag = false;
    let actual_pairs_count = 0;

    for (let i = 0; i < schedule.length; i++) {
        let time = schedule[i]['time'];
        let is_actual_pair;
        let is_now_pair;
        let is_finish_pair;
        let pair = create_pair_schedule(schedule[i], weekday_num, is_actual);
        if (pair)
            actual_pairs_count++;
        if (is_today) {
            let times = time.split('-');
            let date = new Date();
            date.setHours(date.getUTCHours() + config.get('settings.utc'));
            let first_time = times[0].split(':');
            let second_time = times[1].split(':');
            let hours_now = date.getUTCHours();
            let minutes_now = date.getUTCMinutes();
            let hours_pair_to = parseInt(second_time[0]);
            let hours_pair_from = parseInt(first_time[0]);
            let minutes_pair_to = parseInt(second_time[1]);
            let minutes_pair_from = parseInt(first_time[1]);
            is_actual_pair = hours_now < hours_pair_to || hours_now === hours_pair_to && minutes_now <= minutes_pair_to;
            if (is_actual_pair)
                is_now_pair = hours_now > hours_pair_from || hours_now === hours_pair_from && minutes_now >= minutes_pair_from;
            is_finish_pair = is_actual && !is_actual_pair;
        }
        if (pair) {
            flag = true;
            msg += '<pre>' + time;
            if (is_now_pair)
                msg += ' (СЕЙЧАС)';
            if (is_finish_pair)
                msg += ' (ЗАКОНЧЕНА)';
            msg += '</pre>';
            msg += '\n' + pair + '\n';
        }
    }

    return (flag) ? msg : ((actual_pairs_count === 0) ? no_pairs_on_day_message : no_pairs_on_today_message);
};

function create_pair_schedule (pair, weekday_num, is_actual) {
    let msg = '';
    let lessons = pair['lessons'];

    for (let j = 0; j < lessons.length; j++) {
        let lesson = lessons[j];
        let date = new Date();
        date.setHours(date.getUTCHours() + config.get('settings.utc'));
        let weekday_now = utilities.get_weekday(date);
        let new_date = date.getDate() + ((weekday_num >= weekday_now) ? weekday_num - weekday_now : 7 - (weekday_now - weekday_num));
        date.setDate(new_date);
        let lesson_dates = lesson['dates'];
        if (is_actual) {
            if (lesson_dates['date-on'] && !utilities.is_today(date, new Date(lesson_dates['date-on'])))
                continue;
            else {
                if (lesson_dates['date-from'] >= date ||
                    lesson_dates['date-to'] <= date &&
                    !utilities.is_today(date, lesson_dates['date-from']) &&
                    !utilities.is_today(date, lesson_dates['date-to'])
                )
                    continue
            }
        }
        msg += create_lesson_schedule(lesson, is_actual) + '\n';
        if (is_actual)
            break;
    }

    return msg;
}

function create_lesson_schedule (lesson, is_actual) {
    let msg = '';
    msg += '<b>' + lesson['title'] + '</b>\n';
    if (lesson['teachers'])
        msg += '  ' + lesson['teachers'].join(', ') + '\n';
    if (lesson['auditories'])
        msg += '  ' + lesson['auditories'].join(', ');
    if (!is_actual) {
        msg += '\n';
        let dates = lesson['dates'];
        if (dates['date-on']) {
            let date = new Date(dates['date-on']);
            msg += '   ' + date.getDate() + '.' + date.getMonth();
        } else {
            let date_from = new Date(dates['date-from']);
            let date_to = new Date(dates['date-to']);
            msg += '   с ' + date_from.getDate() + '.' + date_from.getMonth() +
                ' по ' + date_to.getDate() + '.' + date_to.getMonth();
        }
    }
    return msg;
}
