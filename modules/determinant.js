const config = require('config');

const weekdays = config.get('enums.weekdays');
const months = config.get('enums.months');

exports.weekday = function(weekday) {
    for (let i = 0; i < weekdays.length; i++)
        for (let [, value] of Object.entries(weekdays[i]))
            if (value.toLowerCase() === weekday.toLowerCase())
                return i;
};

exports.month = function(month) {
    for (let i = 0; i < months.length; i++)
        for (let [, value] of Object.entries(months[i]))
            if (value.toLowerCase() === month.toLowerCase())
                return i;
};