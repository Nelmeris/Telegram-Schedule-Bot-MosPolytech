const config = require('config');
const puppeteer = require('puppeteer');
const HTMLParser = require('node-html-parser');
const schedule_parser = require('../modules/schedule_parser');

exports.schedule_from_server = async function(group) {
    const browser = await puppeteer.launch(config.get('Puppeteer.browser_args'));

    const page = await browser.newPage();
    await page.goto(config.get('MosPolytech.schedule_source_url') + '/?' + group);
    await page.waitForSelector(config.get('HTML_Selector.schedule_week'));
    const content = await page.content();

    await browser.close();

    const html = HTMLParser.parse(content);
    const schedule = html.querySelector(config.get('HTML_Selector.schedule_week'));

    let parsed_schedule = schedule_parser.parse(schedule);

    console.log('The ' + group + ' group schedule has been loaded and updated');
    return {'load_date': Date.now(), 'data': parsed_schedule};
};

exports.group_list_from_server = async function() {
    const fetch = require("node-fetch");
    const url = config.get('MosPolytech.group_list_url');
    const getData = async url => {
        try {
            const response = await fetch(url);
            const json = await response.json();

            console.log('The group list has been loaded and updated');

            return { data: json, update_date: new Date() };
        } catch (error) {
            console.log('Downloading the group list failed. Error: ' + error);
            return null;
        }
    };
    return await getData(url);
};