/** Отключение сообщений об ошибках **/
process.env.NTBA_FIX_319 = 1;
process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';

/** Запуск статического сервера для сайта **/
const _static = require('node-static');
const file = new _static.Server();
require('http').createServer(function(request, response) {
    request.addListener('end', function() {
        file.serve(request, response);
    }).resume();
}).listen(process.env.PORT || 3000);

const config = require('config');

const chats_cache = require('./modules/caching/chats');
const schedules_cache = require('./modules/caching/schedules');

const TelegramBot = require('node-telegram-bot-api');

let is_develop = config.get('settings.is_develop');

const token = config.get('Telegram_Bot.' + ((!is_develop) ? 'token' : 'token_develop'));
const bot = new TelegramBot(token, {polling: true});

const bot_core = require('./modules/bot/handlers');

(async () => {
    await chats_cache.pull_from_db();
    schedules_cache.set_bot(bot);
    await schedules_cache.pull_from_db();
    bot_core.start(bot);
})();
